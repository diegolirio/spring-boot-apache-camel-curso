# Json Server

### Install
```bash
npm install -g json-server
```

### Up
```bash
json-server apis-po.json
```

### Request
```bash
curl --header "Content-Type: application/json" --request POST --data '{"firstname":"Diego Lirio","lastname":"Lirio"}' http://localhost:3000/customers
```
