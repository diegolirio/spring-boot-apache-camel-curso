package io.github.diegolirio.springbootapachelcameludemy.camel.route;


import io.github.diegolirio.springbootapachelcameludemy.camel.processor.CustomerProcessor;
import io.github.diegolirio.springbootapachelcameludemy.model.Customer;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CustomerRoute extends RouteBuilder {

    public static final String DIRECT_ROUTE_POST_CUSTOMER = "direct:http-client-post-customer";
    private static final String ID_DIRECT_ROUTE_POST_CUSTOMER = "direct:http-client-post-customer";

    @Autowired
    private CustomerProcessor customerProcessor;

    @Override
    public void configure() throws Exception {
        from(DIRECT_ROUTE_POST_CUSTOMER)
                .routeId(ID_DIRECT_ROUTE_POST_CUSTOMER)
                .doTry()
                    .process(this.customerProcessor)
                    .log("${body}")
               .marshal().json(JsonLibrary.Jackson, Customer.class)
                .to("http4://localhost:3000/customers")
                .endDoTry()
                .doFinally()
                .setHeader(Exchange.HTTP_RESPONSE_CODE, simple("${in.header.CamelHttpResponseCode}"));
    }
}
