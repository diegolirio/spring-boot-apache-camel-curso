package io.github.diegolirio.springbootapachelcameludemy.camel.rest;

import io.github.diegolirio.springbootapachelcameludemy.model.Customer;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import static io.github.diegolirio.springbootapachelcameludemy.camel.route.CustomerRoute.DIRECT_ROUTE_POST_CUSTOMER;

@Component
public class CustomerRouteRest extends RouteBuilder {

    private static final String DIRECT_ROUTE_REST_POST_CUSTOMER = "direct:customer-post";
    private static final String ID_DIRECT_ROUTE_REST_POST_CUSTOMER = "customer-post";

    @Override
    public void configure() throws Exception {
//        rest("/customers")
//                .post("/")
//                .consumes("application/json")
//                .produces("application/json")
//                .type(Customer.class)
//                .route()
//                .from(DIRECT_ROUTE_REST_POST_CUSTOMER)
//                .routeId(ID_DIRECT_ROUTE_REST_POST_CUSTOMER)
//                .description("Post Customer...")
//                .to(DIRECT_ROUTE_POST_CUSTOMER)
//                .endRest();

        rest("/customers")
                .post().outType(Customer .class)
                .consumes(String.valueOf(MediaType.APPLICATION_JSON))
                .produces(String.valueOf(MediaType.APPLICATION_JSON))
                .route()
                .from(DIRECT_ROUTE_REST_POST_CUSTOMER)
                .to(DIRECT_ROUTE_POST_CUSTOMER)
                .endRest();


    }
}

