package io.github.diegolirio.springbootapachelcameludemy.camel.processor;

import io.github.diegolirio.springbootapachelcameludemy.model.Customer;
import lombok.extern.java.Log;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

@Log
@Component
public class CustomerProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {

        Object body = exchange.getIn().getBody();

        log.info(body+"");

        Customer customer = Customer.builder()
                                        .id("aslckcskdmv09")
                                        .firstname("Diego")
                                        .lastname("Lirio")
                                     .build();

        exchange.getOut().setBody(customer);
        exchange.getOut().removeHeader("CamelHttpUri");
        exchange.getOut().removeHeader("CamelHttpUrl");
        exchange.getOut().removeHeader("CamelHttpPath");
        exchange.getOut().setHeader(Exchange.HTTP_METHOD, "POST");


    }
}
